本项目完全由我本人开发完成，兼容v1.09d/1.10f/1.11b/1.12a/1.13c/1.13d，因为精力有限，我没有详细测试，只能保证都能正常进入游戏，可以多开，可以联网。  
默认根据同目录下的game.exe来判断版本号，你也可以自行修改，特点如下：  
1、兼容v1.09d/1.10f/1.11b/1.12a/1.13c/1.13d  
2、可单机、可联网、可战网  
3、一键安装、一键卸载，因为原版的Storm.dll众所周知的校验，所以要使用自定义loader的话，需要先运行D2LoaderInstall.exe对Storm.dll进行patch，不需要的时候可以用D2LoaderRestore.exe进行恢复  
4、支持原版game.exe的所有命令行参数，比如-w、-direct、-txt、-glide(也就是-3dfx)、-locale、-title等，具体可以看源代码  
5、可以通过参数去除原版game.exe对dep机制的依赖  
  
6、新增若干参数，详细说明如下：  
使用方法，将Install目录下的文件全部复制到游戏根目录，跟game.exe一起，因为这个loader是根据game.exe来自适应游戏版本号的，然后双击D2LoaderInstall.exe对Storm.dll进行patch，就可以正常使用了，安装和卸载方法参照大箱子PlugY。  
-glide 相当于原来的-3dfx  
-notitle 窗口无标题  
-mpq 指定额外的mpq，最多10个，空格分开，可以带路径，路径有空格要用双引号，比如：-mpq Language_CHI\CHI.mpq  
-plugin 指定额外的dll，最多10个，空格分开，可以带路径，路径有空格要用双引号，比如：-plugin d2hackmap\d2hackmap.dll，另外，可以追加plugin的初始化函数，比如：-plugin PlugY.dll:_Init@4  
-mpqpath 指定10 mpqs的加载路径，路径有空格要用双引号，路径的最后不要写反斜杠"\\"，比如：-mpqpath "d:\Diablo II D2SE"  
-dllpath 追加dll的加载路径，路径有空格要用双引号，路径的最后不要写反斜杠"\\"，比如：-dllpath "d:\Diablo II D2SE" "d:\Diablo II D2SE\D2SE\CORES\1.13c"  
-depfix 去除dep的依赖  
-noborder 窗口无边框  
剩下的大家自己看代码吧。